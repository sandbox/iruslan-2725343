# Summary 
Allows to customize page title for pages build with drupal_get_form() as a page
callback (means form as a page).

# Usage
==========
Write in your settings.php:

```
$conf['form_page_title_forms'] = [
  'YOUR_FORM_ID' => 'YOUR TITLE TEXT',
];
```

# Example
```
$conf['form_page_title_forms'] = [
  'user_login' => 'Log In',
  'user_register_form' => 'Sign Up',
  'user_profile_form' => 'Account settings',
];
```
